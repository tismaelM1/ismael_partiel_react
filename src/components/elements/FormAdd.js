import React from 'react'
import {Form,Button } from 'react-bootstrap';


const FormAdd = ({data, setData})=>{


   const sendDataCourse = async (param)=>{

    setData([...data, param]);

    console.log(data)


   }
  
  const handleSubmit = (event) => {
    event.preventDefault();
  
    // const heureDebut = event.target.heureDebut.value;
    const textCourse = event.target.textCourse.value;
  
    const dataForm = {ingredient: textCourse}
    // sendHours(dataForm)
    sendDataCourse(dataForm)
  
  }
  const FormCourse = ({nameForm})=>{
    return (
      <Form.Control size="lg" name={nameForm}/>
    )
  }


    return (
        <Form className="my-3" onSubmit={handleSubmit}>
        <Form.Label>Nom de l'element </Form.Label>
        <FormCourse  nameForm="textCourse"/>  
          <Button type="submit" variant="secondary" className="w-100">Ajouter un éléments</Button>

      </Form>
    )
}

export default FormAdd;