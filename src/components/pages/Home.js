import * as React from "react";
import { Link } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';

import {Card,Container,Row,Col } from 'react-bootstrap';



function Home()  {
    return (
      <>
        <main>
          <h2>Bienvenue sur votre liste de course </h2>
          <p>Ici tu pourra ajouter de nouveau articles et cocher ce que tu possède déjà</p>
        </main>
        <Container className="my-5 py-5">
          <Row className="mt-5">
            <Col>
              <Card bg="secondary">
                <h1>
                  <Link style={styles.link} to="/courses">Je consulte ma liste de course maintenant</Link>
                </h1>
              </Card>
            </Col>
          </Row>
        </Container>
      </>
    );
  }


  export default Home;


  const styles = {
    root: {
      display: "block"
    },
    item: {
      color: "black"
    },
    CardImgOverlay:{
      backgroundColor: 'rgba(0,0,0,.6)'
    },
    link:{
      color:'white',
    }
  }