import React, { useState, useEffect } from 'react';

import { Row, Col, Form } from 'react-bootstrap';
import { Link } from "react-router-dom";

import FormAdd from './..//elements/FormAdd'

// import datalist from "./../datalist.js";




function Courses() {
  const [data, setData] = useState([]);


  useEffect(() => {
    const dataJson = [
      { 
        ingredient: "salade de tomates " ,
        // check: false
      },
      { ingredient: "oeuf" },
      { ingredient: "fromages" },
    ];
      setData(dataJson)
  }, []);

  const handleChecked = async (id) => {
    // event.preventDefault();
    console.log('okayyy chequed ')
    // let id = event.target.deleteId.value
    // data[id].hours = []
    const tempDataForDelete = data.slice();

    if (tempDataForDelete[id].check) {
      tempDataForDelete[id].check = false
    } else {
      tempDataForDelete[id].check = true

    }
    setData(tempDataForDelete)

    console.log(data)
  }
  return (
    <>

      <main className="container">
        <h2>Bienvenue sur votre liste de course </h2>
        <ul className='list-unstyled'>
          {data.map((item, index) => (
              <li key={index} className="d-flex">
              <Form.Check onClick={()=>{handleChecked(index)}}
                type="checkbox"
                className="pe-3"
              />
              {
                item.check === true ? <strike>{item.ingredient}</strike> : item.ingredient
              }
                
              </li>
          ))}
        </ul>



        <Row className="mb-5  ">
          <Col>
            <FormAdd data={data} setData={setData} />
          </Col>
        </Row>
        <nav>
          <Link to="/" className=" fs-3">Retourner a la page d'acceuil</Link>
        </nav>
      </main>


    </>
  );
}


export default Courses;
