import * as React from "react";
import { Routes, Route } from "react-router-dom";
import "./App.css";
import 'bootstrap/dist/css/bootstrap.min.css';

// import { Button,Card,Container,Row,Col,Table } from 'react-bootstrap';
import styled from '@emotion/styled'

import Courses from './components/pages/Courses'
import Home from './components/pages/Home'




function App() {
  return (
    <div className="App" 
    
    >
      <Header>
        <h1>Partiels de Ismael HINI M1 TL REACT APP</h1>

      </Header>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="courses" element={<Courses />} />
      </Routes>
    </div>
  );
}

export default App;

const Header = styled.div`
  background: black;
  color: #fff;
`
// App.js






// const styles = {
//   root: {
//     display: "block"
//   },
//   item: {
//     color: "black"
//   },
//   CardImgOverlay:{
//     backgroundColor: 'rgba(0,0,0,.6)'
//   },
//   link:{
//     color:'white',
//   }
// }